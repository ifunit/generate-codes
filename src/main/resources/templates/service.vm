/**
 * Copyright (c) 2012-2013 All Rights Reserved.
 */
package ${package};

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import ${daoTask.properties.package}.${daoTask.prefix}${entity}${daoTask.suffix};
import ${entityTask.properties.package}.${entityTask.prefix}${entity}${entityTask.suffix};
import com.ifunit.core.common.Page;

/**
 * $!{entityComment}Service
 * 
 * @author ${author}
 * @version v 0.1 ${now} ${author} Exp $
 */
@Service
public class ${task.prefix}$!{entity}${task.suffix} {

    @Autowired
    private ${daoTask.prefix}${entity}${daoTask.suffix} ${entityDefine}${daoTask.suffix};

    /**
     * 保存$!{entityComment},根据ID判断是保存还是更新
     * 
     * @param $!{entityDefine}
     * @return 
     */
    public ${entity} save(${entity} $!{entityDefine}) {
        if ($!{entityDefine}.get$!{name.getFirstUpper($name.name($autoPrimaryColumn.column))}() == null) {
            ${entityDefine}${daoTask.suffix}.insertSelective($!{entityDefine});
        } else {
            ${entityDefine}${daoTask.suffix}.updateSelective($!{entityDefine});
        }
        return $!{entityDefine};
    }

    /**
     * 根据主键删除
     * 
     * @param keys
     * @return
     */
    public void delete(#foreach($item in $primaryList)$!{item.typeDefine}[]#end keys) {
        ${entityDefine}${daoTask.suffix}.delete(keys);
    }

    /**
     * 根据主键查询
     * 
     * @param #foreach($item in $primaryList) $!{name.getFirstLower($name.name($item.column))} 
     #end
     * @return
     */
    public ${entity} get(#foreach($item in $primaryList)$!{item.typeDefine} $!{name.getFirstLower($name.name($item.column))}#end) {

        return ${entityDefine}${daoTask.suffix}.get(id);
    }

    /**
     * 分页查询
     * 
     * @param page
     */
    public void page(Page<${entity}> page) {

    	RowBounds rowBounds = new RowBounds(page.getOffset(), page.getPageSize());
    	Map<String, Object> criteria = page.getCriteria();
    	criteria.put("keyword", page.getKw());
        List<${entity}> list = ${entityDefine}${daoTask.suffix}.select(criteria, rowBounds);
        page.setList(list);
        page.setTotalCount(${entityDefine}${daoTask.suffix}.count(criteria));
    }

}
