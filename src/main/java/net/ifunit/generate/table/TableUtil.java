/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.table;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import net.ifunit.generate.db.DatabaseProvider;
import net.ifunit.generate.model.Column;
import net.ifunit.generate.model.PrimaryType;
import net.ifunit.generate.model.Table;
import net.ifunit.generate.util.ConfigPropertiesUtil;

import org.apache.log4j.Logger;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午12:42:02 wy Exp $
 */
public class TableUtil {

    /** 日志对象 */
    private static final Logger LOG = Logger.getLogger(TableUtil.class);

    /**
     * 获取表的信息
     * 
     * @param tableName
     * @return
     */
    public static Table getTable(String tableName) {
        String driverClass = ConfigPropertiesUtil.get("db.provider");
        Object provider = null;
        try {
            provider = Class.forName(driverClass).newInstance();
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException("找不到类" + driverClass);
        }
        catch (Exception e) {
            throw new RuntimeException("初始化" + driverClass + "异常", e);
        }

        if (!(provider instanceof DatabaseProvider)) {
            throw new RuntimeException("" + driverClass + "类型不正确");
        }

        LOG.info("DatabaseProvider class " + driverClass);

        DatabaseProvider databaseProvider = (DatabaseProvider) provider;

        String tableComment = databaseProvider.getTableComment(tableName);

        LOG.info("数据库表" + tableName + "的注释为:" + tableComment);

        List<Column> columnList = databaseProvider.getTableFields(tableName);

        LOG.info("数据库表" + tableName + "的字段共有:" + columnList.size() + "个");
        List<String> primaryList = databaseProvider.getPrimaryColumns(tableName);

        LOG.info("数据库表" + tableName + "的主键共有:" + primaryList.size() + "个");
        LinkedHashMap<String, String> foreignMap = databaseProvider.getForeignColumns(tableName);
        LOG.info("数据库表" + tableName + "的外键共有:" + foreignMap.size() + "个");

        Table table = new Table();

        table.setName(tableName);

        table.setDesc(tableComment);

        List<Column> primaryColumnList = new ArrayList<Column>();

        if (primaryList == null || primaryList.isEmpty()) {
            table.setPrimaryType(PrimaryType.NONE);
        } else if (primaryList.size() == 1) {
            table.setPrimaryType(PrimaryType.ONE);
        } else {
            table.setPrimaryType(PrimaryType.MANY);
        }

        List<Column> notPrimaryColumnList = new ArrayList<Column>();
        
        for (Column column : columnList) {
            if (primaryList.contains(column.getColumn())) {
                primaryColumnList.add(column);
                column.setPrimary(true);
            }
            if (foreignMap.containsKey(column.getColumn())) {
                column.setForeign(true);
            }
            if (!column.isPrimary()) {
                notPrimaryColumnList.add(column);
            }
        }

        table.setPrimaryList(primaryColumnList);

        table.setForeignMap(foreignMap);

        //        for (Column column : columnList) {
        //            column.setPrimary(primaryList.contains(column.getColumn()));
        //            column.setForeign(foreignMap.keySet().contains(column.getColumn()));
        //            if (!primaryList.contains(column.getColumn())) {
        //                
        //            }
        //        }

        table.setNotPrimaryList(notPrimaryColumnList);
        table.setColumnList(columnList);

        return table;
    }

}
