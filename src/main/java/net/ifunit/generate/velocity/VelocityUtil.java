/**
 * Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午9:40:26 wy Exp $
 */
public class VelocityUtil {
    /** 日志 */
    private final static Logger   log      = LoggerFactory.getLogger(VelocityUtil.class);

    /** 默认模版文件夹 */
    private final static String   TEMPLATE = "templates";

    /** 这个需要重新实例化一个 */
    private static VelocityEngine VELOCITY_ENGINE;

    /**
     * 实例化
     */
    public static VelocityEngine getInstance() {

        if (VELOCITY_ENGINE != null) {
            return VELOCITY_ENGINE;
        }
        init();
        return VELOCITY_ENGINE;
    }

    /**
     * 初始化
     */
    public static void init() {
        if (VELOCITY_ENGINE != null) {
            return;
        }
        try {
            Properties properties = new Properties();
            properties.load(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("velocity.properties"));
            String path = properties.getProperty(Velocity.FILE_RESOURCE_LOADER_PATH);
            if (path == null) {
                properties.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, Thread.currentThread()
                    .getContextClassLoader().getResource("").toURI().getPath()
                                                                           + TEMPLATE + "/");
            }
            VELOCITY_ENGINE = new VelocityEngine(properties);
        }
        catch (Exception e) {
            log.error("velocity初始化失败", e);
            throw new RuntimeException("velocity初始化失败");
        }
    }

    /**
     * 通过模版返回html代码
     * 
     * @param pathname
     *            模版路径
     * @param encoding
     * @param params
     * @return
     */
    public static StringBuffer create(String pathname, String encoding, Map<String, Object> params) {
        log.info("根据{}生成..", pathname);
        VelocityEngine velocity = getInstance();
        File vmFile = new File(velocity.getProperty(Velocity.FILE_RESOURCE_LOADER_PATH) + pathname);
        if (!vmFile.exists()) {
            throw new RuntimeException("找不到" + pathname + "的vm");
        }
        StringWriter sw = new StringWriter();
        try {
            Template template = velocity.getTemplate(pathname, encoding);
            VelocityContext context = contextInit(params);
            template.merge(context, sw);
            return sw.getBuffer();
        }
        catch (ResourceNotFoundException e) {
            log.error("转换html的velocity的文件找不到", e);
        }
        catch (ParseErrorException e) {
            log.error("转换html的velocity的格式化错误", e);
            e.printStackTrace();
        }
        catch (Exception e) {
            log.error("转换html的velocity错误", e);
        }
        finally {
            try {
                sw.close();
            }
            catch (IOException e) {
                log.error("velocity的StringWriter关闭异常");
            }
        }
        throw new RuntimeException("转换html的velocity错误");
    }

    /**
     * 通过模版返回html代码
     * 
     * @param template
     *            模版字符串
     * @param params
     * @return
     */
    public static StringBuffer create(String template, Map<String, Object> params) {

        StringWriter sw = new StringWriter();
        try {
            //初始化上下文
            VelocityContext context = contextInit(params);
            getInstance().evaluate(context, sw, "", template);
            return sw.getBuffer();
        }
        catch (ResourceNotFoundException e) {
            log.error("转换html的velocity的资源文件找不到", e);
        }
        catch (ParseErrorException e) {
            log.error("转换html的velocity的格式化错误", e);
        }
        catch (Exception e) {
            log.error("转换html的velocity错误", e);
        }
        finally {
            try {
                sw.close();
            }
            catch (IOException e) {
                log.error("velocity的StringWriter关闭异常");
            }
        }
        throw new RuntimeException("转换html的velocity错误");
    }

    /**
     * 初始化数据
     * 
     * @param params
     * @return
     */
    private static VelocityContext contextInit(Map<String, Object> params) {
        VelocityContext context = new VelocityContext();
        if (params != null && params.size() > 0) {
            for (Entry<String, Object> entry : params.entrySet()) {
                context.put(entry.getKey(), entry.getValue());
            }
        }
        return context;
    }

    /**
     * 通过模版返回html代码
     * 
     * @param pathname
     * @param params
     * @return
     */
    public static StringBuffer createUTF8(String pathname, Map<String, Object> params) {
        return create(pathname, "UTF-8", params);
    }
}
