/**
 * Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate;

import net.ifunit.generate.task.Task;
import net.ifunit.generate.task.TaskUtil;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午9:40:26 wy Exp $
 */
public class Main {

    public static void main(String[] args) {
        TaskUtil.load();
        String[] tables = TaskUtil.getTables();
        for (String table : tables) {
            for (Task task : TaskUtil.getTaskmap().values()) {
                task.getGenerateTask().generate(task, table);
            }
        }

    }
}
