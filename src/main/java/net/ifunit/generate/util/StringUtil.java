/**
 * Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.util;

import net.ifunit.generate.task.TaskUtil;

import org.apache.commons.lang.math.RandomUtils;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午9:40:26 wy Exp $
 */
public class StringUtil {

	/**
	 * 首字母大写
	 * 
	 * @param name
	 * @return
	 */
	public static String getFirstUpper(String name) {
		name = name.trim();
		return name.substring(0, 1).toUpperCase() + (name.length() > 1 ? name.substring(1) : "");
	}

	/**
	 * 首字母小写
	 * 
	 * @param name
	 * @return
	 */
	public static String getFirstLower(String name) {
		name = name.trim();
		return name.substring(0, 1).toLowerCase() + (name.length() > 1 ? name.substring(1) : "");
	}

	/**
	 * 默认的名称转换
	 * 
	 * @param name
	 * @return
	 */
	public static String name(String name) {

		if (name.startsWith(TaskUtil.getTablePrefix())) {
			name = name.substring(TaskUtil.getTablePrefix().length());
		}
		if (name.indexOf("_") == -1) {
			return name;
		}

		String[] names = name.toLowerCase().split("_");
		StringBuilder sb = new StringBuilder();
		for (String n : names) {
			if (n.trim().length() == 0) {
				continue;
			}
			sb.append(getFirstUpper(n.trim()));
		}
		return sb.toString();
	}

	/**
	 * 路径转换
	 * 
	 * @param name
	 * @return
	 */
	public static String path(String name) {

		name = getFirstLower(name);

		char[] chs = name.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char ch : chs) {
			if (ch >= 'A' && ch <= 'Z') {
				if (sb.lastIndexOf("_") != sb.length() - 1) {
					sb.append("_");
				}
				sb.append((char) (ch + 'a' - 'A'));
			} else {
				sb.append(ch);
			}
		}

		return sb.toString();
	}

	/**
	 * 随机的序列化Id
	 * 
	 * @return
	 */
	public static String getSerialVersionUID() {
		String serialVersionUID = RandomUtils.nextLong() + "";
		while (serialVersionUID.length() < 19) {
			serialVersionUID += RandomUtils.nextLong() + "";
		}
		return (System.currentTimeMillis() % 3 == 2 ? "-" : "") + serialVersionUID.substring(0, 18) + "L";
	}

}
