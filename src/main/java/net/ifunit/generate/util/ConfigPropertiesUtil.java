/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.util;

import java.util.ResourceBundle;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午1:36:07 wy Exp $
 */
public class ConfigPropertiesUtil {

    /**
     * 获取配置文件
     * 
     * @param key
     * @return
     */
    public static String get(String key) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        return resourceBundle.getString(key);
    }
}
