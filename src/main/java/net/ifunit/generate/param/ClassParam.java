/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.param;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午11:00:34 wy Exp $
 */
public class ClassParam {

    private String className;

    public ClassParam(String className) {
        this.className = className;
    }

    /**
     * 对象对应
     * 
     * @return
     */
    public String getDefine() {
        return className.substring(className.lastIndexOf("."));
    }

    /**
     * 包引入
     * 
     * @return
     */
    public String getImport() {
        return "import " + className;
    }

    /**
     * Getter method for property <tt>className</tt>.
     * 
     * @return property value of className
     */
    public String getClassName() {
        return className;
    }

}
