/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.model;

/**
 * 字段
 * 
 * @author wy
 * @version v 0.1 2013-6-1 上午9:54:15 wy Exp $
 */
public class Column implements Comparable<Column> {

    //保证多主键字段顺序
    static int order = 0;

    /** 字段名称 */
    private String column;

    /** 对应的java.sql.Type */
    private int type;

    /** 对应的java类型 */
    private Class<?> typeClass;

    /** 是否主键 */
    private boolean isPrimary = false;

    /** 是否外键 */
    private boolean isForeign = false;

    /** 字段注释 */
    private String comment;

    /** 字段长度 */
    private int length;

    /** 是否自增 */
    private boolean autoincrement;

    /** 是否能为空 */
    private boolean nullable;

    /** 类型名称 */
    private String typeName;

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Column o) {
        if (o == null) {
            return 1;
        }
        if (isPrimary && !o.isPrimary) {
            return -1 - ++order;
        }
        return 0;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Class<?> getTypeClass() {
        return typeClass;
    }

    public void setTypeClass(Class<?> typeClass) {
        this.typeClass = typeClass;
    }

    /**
     * 类型定义
     * 
     * @return
     */
    public String getTypeDefine() {
        return typeClass.getSimpleName();
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isAutoincrement() {
        return autoincrement;
    }

    public void setAutoincrement(boolean autoincrement) {
        this.autoincrement = autoincrement;
    }

    public boolean isForeign() {
        return isForeign;
    }

    public void setForeign(boolean isForeign) {
        this.isForeign = isForeign;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    
    

}
