/**
 * 
 */
package net.ifunit.generate.task;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * 代码任务
 * 
 * @author wy
 * @version v 0.1 2013-6-2 下午1:36:07 wy Exp $
 */
public class Task {

    /** 名称 */
    private String              name;

    /** 使用的模板 */
    private String              template;

    /** 文件名后缀 */
    private String              suffix;

    /** 文件名前缀 */
    private String              prefix;

    /** 文件扩展名 */
    private String              ext;

    /** 路径 */
    private String              path;

    /** 生成任务 */
    private GenerateTask        generateTask = new DefaultGenerateTask();

    /** 配置的属性 */
    private Map<String, String> properties;

    /**
     * Getter method for property <tt>name</tt>.
     * 
     * @return property value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property <tt>name</tt>.
     * 
     * @param name
     *            value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for property <tt>template</tt>.
     * 
     * @return property value of template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * Setter method for property <tt>template</tt>.
     * 
     * @param template
     *            value to be assigned to property template
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * Getter method for property <tt>suffix</tt>.
     * 
     * @return property value of suffix
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Setter method for property <tt>suffix</tt>.
     * 
     * @param suffix
     *            value to be assigned to property suffix
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * Getter method for property <tt>prefix</tt>.
     * 
     * @return property value of prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Setter method for property <tt>prefix</tt>.
     * 
     * @param prefix
     *            value to be assigned to property prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Getter method for property <tt>ext</tt>.
     * 
     * @return property value of ext
     */
    public String getExt() {
        return ext;
    }

    /**
     * Setter method for property <tt>ext</tt>.
     * 
     * @param ext
     *            value to be assigned to property ext
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * Getter method for property <tt>path</tt>.
     * 
     * @return property value of path
     */
    public String getPath() {
        return path;
    }

    /**
     * Setter method for property <tt>path</tt>.
     * 
     * @param path
     *            value to be assigned to property path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Getter method for property <tt>generateTask</tt>.
     * 
     * @return property value of generateTask
     */
    public GenerateTask getGenerateTask() {
        return generateTask;
    }

    /**
     * Setter method for property <tt>generateTask</tt>.
     * 
     * @param generateTask
     *            value to be assigned to property generateTask
     */
    public void setGenerateTask(GenerateTask generateTask) {
        this.generateTask = generateTask;
    }

    /**
     * Getter method for property <tt>properties</tt>.
     * 
     * @return property value of properties
     */
    public Map<String, String> getProperties() {
        return properties;
    }

    /**
     * Setter method for property <tt>properties</tt>.
     * 
     * @param properties
     *            value to be assigned to property properties
     */
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    /**
     * 验证数据
     */
    public void valid() {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("name 不能为空");
        }
        if (StringUtils.isBlank(template)) {
            throw new IllegalArgumentException("template 不能为空");
        }
        if (StringUtils.isBlank(ext)) {
            throw new IllegalArgumentException("ext 不能为空");
        }
        if (StringUtils.isBlank(path)) {
            throw new IllegalArgumentException("path 不能为空");
        }
    }

}
