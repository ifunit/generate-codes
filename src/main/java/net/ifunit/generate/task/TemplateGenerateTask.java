/**
 * Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.task;

import java.io.File;

import net.ifunit.generate.util.StringUtil;


/**
 * 模板页面生成
 * 
 * @author wy
 * @version v 0.1 2013-9-23 下午6:21:24 wy Exp $
 */
public class TemplateGenerateTask extends DefaultGenerateTask {
    /**
     * 文件保存路径
     * 
     * @param task
     * @param entity
     * @return
     */
    protected String getFilePath(Task task, String entity) {
        return task.getPath() + File.separator + task.getPrefix() + StringUtil.path(entity)
               + task.getSuffix() + "." + task.getExt();
    }
}
