/**
 * Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.task;

/**
 * 生成任务
 * 
 * @author wy
 * @version v 0.1 2013-9-23 下午12:42:36 wy Exp $
 */
public interface GenerateTask {

    /**
     * 生成任务
     * 
     * @param task
     * @param tableName
     */
    public void generate(Task task, String tableName);

}
