/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import net.ifunit.generate.db.jdbc.JdbcUtil;
import net.ifunit.generate.db.type.ColumnTypeMapUtil;
import net.ifunit.generate.model.Column;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 上午9:46:15 wy Exp $
 */
public class JdbcProvider implements DatabaseProvider {

    /** 日志对象 */
    protected final Logger LOG = Logger.getLogger(getClass());

    /**
     * @see net.ifunit.generate.db.DatabaseProvider#getTableComment(java.lang.String)
     */
    @Override
    public String getTableComment(String tableName) {
        Connection conn = JdbcUtil.getConnection();
        ResultSet rs = null;
        String comment = null;
        try {
            DatabaseMetaData meta = conn.getMetaData();
            rs = meta.getTables(null, null, tableName, new String[] { "VIEW", "TABLE" });
            while (rs.next()) {
                comment = rs.getString("REMARKS");
                break;
            }
        }
        catch (SQLException e) {
            LOG.error("获取表注释异常", e);
            throw new RuntimeException("获取表注释异常");
        }
        finally {
            JdbcUtil.close(rs);
            JdbcUtil.close(conn);
        }
        return comment;
    }

    /**
     * @see net.ifunit.generate.db.DatabaseProvider#getTableFields(java.lang.String)
     */
    @Override
    public List<Column> getTableFields(String tableName) {

        Connection conn = JdbcUtil.getConnection();
        ResultSet rs = null;
        List<Column> list = new ArrayList<Column>();
        try {
            DatabaseMetaData meta = conn.getMetaData();
            rs = meta.getColumns(null, null, tableName, null);
            while (rs.next()) {
                int count = rs.getMetaData().getColumnCount();
                System.out.println("--------------------------------");
                while (count > 2) {
                    System.out.print(rs.getMetaData().getColumnName(count) + " ");
                    ;
                    System.out.println(rs.getObject(count--));
                }
                String name = rs.getString("COLUMN_NAME");
                String typeName = rs.getString("TYPE_NAME");
                int nullable = rs.getInt("NULLABLE");
                int size = rs.getInt("COLUMN_SIZE");
                String comment = rs.getString("REMARKS");
                boolean autoincrement = BooleanUtils.toBoolean(rs.getString("IS_AUTOINCREMENT"));
                int type = rs.getInt("DATA_TYPE");
                Class<?> clazz = ColumnTypeMapUtil.get(type, size);
                Column column = new Column();
                column.setComment(comment);
                column.setColumn(name);
                column.setLength(size);
                column.setTypeClass(clazz);
                column.setType(type);
                column.setAutoincrement(autoincrement);
                column.setTypeName(typeName);
                column.setNullable(nullable > 0);
                list.add(column);
            }
        }
        catch (SQLException e) {
            LOG.error("获取表的字段出错", e);
            throw new RuntimeException("获取表的字段出错");
        }
        finally {
            JdbcUtil.close(rs);
            JdbcUtil.close(conn);
        }
        return list;
    }

    /**
     * @see net.ifunit.generate.db.DatabaseProvider#getPrimaryColumns(java.lang.String)
     */
    @Override
    public List<String> getPrimaryColumns(String tableName) {
        Connection conn = JdbcUtil.getConnection();
        ResultSet rs = null;
        List<String> list = new ArrayList<String>();
        try {
            DatabaseMetaData meta = conn.getMetaData();
            rs = meta.getPrimaryKeys(null, null, tableName);
            while (rs.next()) {
                String column = rs.getString("COLUMN_NAME");
                list.add(column);
            }
        }
        catch (SQLException e) {
            LOG.error("获取表的主键出错", e);
            throw new RuntimeException("获取表的主键出错");
        }
        finally {
            JdbcUtil.close(rs);
            JdbcUtil.close(conn);
        }
        return list;
    }

    /**
     * @see net.ifunit.generate.db.DatabaseProvider#getForeignColumns(java.lang.String)
     */
    @Override
    public LinkedHashMap<String, String> getForeignColumns(String tableName) {
        Connection conn = JdbcUtil.getConnection();
        ResultSet rs = null;

        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        try {
            DatabaseMetaData meta = conn.getMetaData();
            rs = meta.getImportedKeys(null, null, tableName);
            while (rs.next()) {
                String column = rs.getString("FKCOLUMN_NAME");
                String table = rs.getString("FKTABLE_NAME");
                map.put(column, table);
            }
        }
        catch (SQLException e) {
            LOG.error("获取表的外键出错", e);
            throw new RuntimeException("获取表的外键出错");
        }
        finally {
            JdbcUtil.close(rs);
            JdbcUtil.close(conn);
        }
        return map;
    }

}
