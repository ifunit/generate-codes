/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.db.type;

import java.util.HashMap;

/**
 * 匹配的Map
 * 
 * @author wy
 * @version v 0.1 2013-6-2 上午10:52:55 wy Exp $
 */
public class TypeMatchMap extends HashMap<TypeMatch, Class<?>> {

    /**  */
    private static final long serialVersionUID = -3492357587848641940L;

    /**
     * 获取对象
     * 
     * @param type
     * @param size
     * @return
     */
    public Class<?> get(int type, int size) {
        for (TypeMatch match : super.keySet()) {
            if (match.getType() == type && size >= match.getMinSize() && size <= match.getMaxSize()) {
                return super.get(match);
            }
        }
        return null;
    }

}
