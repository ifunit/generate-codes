/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.db.type;

/**
 * 
 * @author wy
 * @version v 0.1 2013-6-2 上午10:47:15 wy Exp $
 */
public class TypeMatch {

    /** 对应的java.sql.Types */
    private int type;

    /** 最大长度 */
    private int minSize;

    /** 最小长度 */
    private int maxSize;

    public TypeMatch(int type) {
        this.type = type;
        this.minSize = -1;
        this.maxSize = Integer.MAX_VALUE;
    }

    public TypeMatch(int type, int size) {
        this.type = type;
        this.minSize = size;
        this.maxSize = size;
    }

    public TypeMatch(int type, int minSize, int maxSize) {
        this.type = type;
        this.minSize = minSize;
        this.maxSize = maxSize;
    }

    /**
     * Getter method for property <tt>type</tt>.
     * 
     * @return property value of type
     */
    public int getType() {
        return type;
    }

    /**
     * Setter method for property <tt>type</tt>.
     * 
     * @param type
     *            value to be assigned to property type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Getter method for property <tt>minSize</tt>.
     * 
     * @return property value of minSize
     */
    public int getMinSize() {
        return minSize;
    }

    /**
     * Setter method for property <tt>minSize</tt>.
     * 
     * @param minSize
     *            value to be assigned to property minSize
     */
    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    /**
     * Getter method for property <tt>maxSize</tt>.
     * 
     * @return property value of maxSize
     */
    public int getMaxSize() {
        return maxSize;
    }

    /**
     * Setter method for property <tt>maxSize</tt>.
     * 
     * @param maxSize
     *            value to be assigned to property maxSize
     */
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + maxSize;
        result = prime * result + minSize;
        result = prime * result + type;
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TypeMatch other = (TypeMatch) obj;
        if (maxSize != other.maxSize) {
            return false;
        }
        if (minSize != other.minSize) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        return true;
    }

}
