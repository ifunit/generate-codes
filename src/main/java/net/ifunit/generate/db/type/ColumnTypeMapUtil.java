/**
 * cxybj.com Copyright (c) 2012-2013 All Rights Reserved.
 */
package net.ifunit.generate.db.type;

import static java.sql.Types.*;

/**
 * 数据库字段映射
 * 
 * @author wy
 * @version v 0.1 2013-6-2 上午12:39:19 wy Exp $
 */
public class ColumnTypeMapUtil {

    private final static TypeMatchMap MAP = new TypeMatchMap();

    /**
     * 获取对应的class
     * 
     * @param type
     * @param size
     * @return
     */
    public static Class<?> get(int type, int size) {
        Class<?> clazz = MAP.get(type, size);
        return clazz;
    }

    static {
        MAP.put(new TypeMatch(VARCHAR), java.lang.String.class);
        MAP.put(new TypeMatch(CHAR), java.lang.String.class);
        MAP.put(new TypeMatch(LONGVARCHAR), java.lang.String.class);
        MAP.put(new TypeMatch(NUMERIC), java.math.BigDecimal.class);
        MAP.put(new TypeMatch(DECIMAL), java.math.BigDecimal.class);
        MAP.put(new TypeMatch(BIT, 0, 1), java.lang.Boolean.class);
        MAP.put(new TypeMatch(BIT, 2, Integer.MAX_VALUE), byte[].class);
        MAP.put(new TypeMatch(TINYINT, 1), java.lang.Boolean.class);
        MAP.put(new TypeMatch(TINYINT, 2, Integer.MAX_VALUE), java.lang.Integer.class);
        MAP.put(new TypeMatch(SMALLINT, 1), java.lang.Boolean.class);
        MAP.put(new TypeMatch(SMALLINT, 2, Integer.MAX_VALUE), java.lang.Short.class);
        MAP.put(new TypeMatch(INTEGER), java.lang.Integer.class);
        MAP.put(new TypeMatch(BIGINT), java.lang.Long.class);
        MAP.put(new TypeMatch(REAL), java.lang.Float.class);
        MAP.put(new TypeMatch(FLOAT), java.lang.Float.class);
        MAP.put(new TypeMatch(DOUBLE), java.lang.Double.class);
        MAP.put(new TypeMatch(BINARY), byte[].class);
        MAP.put(new TypeMatch(VARBINARY), byte[].class);
        MAP.put(new TypeMatch(LONGVARBINARY), byte[].class);
        MAP.put(new TypeMatch(DATE), java.sql.Date.class);
        MAP.put(new TypeMatch(TIME), java.sql.Time.class);
        MAP.put(new TypeMatch(TIMESTAMP), java.util.Date.class);
        MAP.put(new TypeMatch(BLOB), byte[].class);
    }
}
